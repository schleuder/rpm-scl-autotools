%{?_compat_el5_build}

%{!?scl:%global scl autotools-git}

%{?scl:%scl_package m4}

%global upstream_stamp 678-c9436


Summary: The GNU macro processor
Name: %{scl_prefix}m4
Version: 1.9a
Release: 8.%(echo %upstream_stamp | sed 's|-|_|')%{?dist}
License: GPLv3+
Group: Applications/Text
Source0: m4-%{version}.%{upstream_stamp}.tar.gz
URL: http://www.gnu.org/software/m4/
Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info
BuildRequires: texinfo, gettext

# Gnulib bundled - the library has been granted an exception, see https://fedorahosted.org/fpc/ticket/174
# Gnulib is not versioned, see m4 ChangeLog for approximate date of Gnulib copy
Provides: bundled(gnulib)

%{?scl:
BuildRequires: scl-utils-build
Requires:%scl_runtime
}


%description
A GNU implementation of the traditional UNIX macro processor.  M4 is
useful for writing text files which can be logically parsed, and is used
by many programs as part of their build process.  M4 has built-in
functions for including files, running shell commands, doing arithmetic,
etc.  The autoconf program needs m4 for generating configure scripts, but
not for running configure scripts.

Install m4 if you need a macro processor.

%prep
%setup -q -n m4-%{version}.%upstream_stamp
chmod 644 COPYING

%build
%configure --disable-rpath --disable-static
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}


%install
make install DESTDIR=$RPM_BUILD_ROOT
rm -f $RPM_BUILD_ROOT%{_infodir}/dir
%find_lang m4
find $RPM_BUILD_ROOT -name '*.la' -exec rm {} +


%check
export LD_LIBRARY_PATH=`pwd`/`find -name .libs | grep 'm4/.libs'`
make %{?_smp_mflags} check
unset LD_LIBRARY_PATH


%files -f m4.lang
%doc AUTHORS COPYING ChangeLog NEWS README TODO
%{_bindir}/m4
%{_infodir}/*
%{_mandir}/man1/m4.1*
%{_datadir}/m4
%{_libdir}/m4
%{_libdir}/*.so*
%{_includedir}/m4


%post
if [ -f %{_infodir}/m4.info.gz ]; then # --excludedocs?
    /sbin/install-info %{_infodir}/m4.info.gz %{_infodir}/dir || :
fi


%preun
if [ "$1" = 0 ]; then
    if [ -f %{_infodir}/m4.info.gz ]; then # --excludedocs?
        /sbin/install-info --delete %{_infodir}/m4.info.gz %{_infodir}/dir || :
    fi
fi

%changelog
* Mon Oct 10 2016 Pavel Raiskup <praiskup@redhat.com> - 1.9a-8.678_c9436
- bump: rebuild october 2016

* Tue Sep 27 2016 Pavel Raiskup <praiskup@redhat.com> - 1.9a-6.678_c9436
- bump, use epel-rpm-macros

* Thu Jun 16 2016 Pavel Raiskup <praiskup@redhat.com> - 1.9a-5.678_c9436
- update to latest HEAD

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 1.9a-5.645_0dc1
- use _compat_el5_build only if defined (rhbz#1252751)

* Thu May 29 2014 Pavel Raiskup <praiskup@redhat.com> - 1.9a-4.645_0dc1
- new tarball with applied downstream patches (to enable testsuite)

* Thu May 29 2014 Pavel Raiskup <praiskup@redhat.com> - 1.9a-3.644_c090
- use %%_compat_el5_build

* Thu May 29 2014 Pavel Raiskup <praiskup@redhat.com> - 1.9a-1.644_c090
- fix load failures by really proposed patch, spec lint
- remove SOURCE1 signature file, not available for git snapshot
- touching *.info file requires regenerate docs (BR texinfo)

* Wed May 28 2014 Pavel Raiskup <praiskup@redhat.com> - 1.9a-1.644_c090
- move to git m4 version

* Mon Mar 24 2014 Pavel Raiskup <praiskup@redhat.com> - 1.4.17-1
- SCLized spec file from rawhide
