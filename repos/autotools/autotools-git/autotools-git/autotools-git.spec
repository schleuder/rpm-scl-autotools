%{?_compat_el5_build}

# Workaround to always have %%scl defined
%{!?scl:%global scl autotools-git}

%{?scl_package:%scl_package %{scl}}

Summary: Package that installs %scl
Name: %scl
Version: 1
Release: 23%{?dist}
License: GPLv2+
Group: Applications/File

Requires: %{?scl_prefix}m4 %{?scl_prefix}automake
Requires: %{?scl_prefix}autoconf %{?scl_prefix}libtool
# Fix build even when scl-utils-build is not in minimal buildroot.
BuildRequires: scl-utils-build


%description
This is the main package for %scl Software Collection.  It contains the latest
development (unstable) versions of autotools.  Just run "scl enable %scl bash"
to make it work instead of system-default autotools.


%package runtime
Summary: Package that handles %scl Software Collection.
Group: Applications/File
Requires: scl-utils

%description runtime
Package shipping essential scripts to work with %scl Software Collection.


%package build
Summary: Package that handles %{scl} Software Collection.
Group: Applications/File
Requires: scl-utils-build
Requires: compat-rpm-config
Requires: scl-rpm-config

%description build
Package shipping essential configuration macros to build %{scl} Software
Collection or packages depending on %{scl} Software Collection.


%prep
%setup -c -T


%build
%if 0%{?rhel} >= 5 && 0%{?rhel} < 8
cat > enable <<\EOF
%_compat_scl_env_adjust PATH            %{_bindir}
%_compat_scl_env_adjust LIBRARY_PATH    %{_libdir}
%_compat_scl_env_adjust LD_LIBRARY_PATH %{_libdir}
%_compat_scl_env_adjust MANDIR          %{_mandir}
%_compat_scl_env_adjust INFOPATH        %{_infodir}
EOF
%else
cat > %{scl} << EOF
#%%Module1.0
prepend-path    X_SCLS              %{scl}
prepend-path    PATH                %{_bindir}
prepend-path    LIBRARY_PATH        %{_libdir}
prepend-path    LD_LIBRARY_PATH     %{_libdir}
prepend-path    MANPATH             %{_mandir}
prepend-path    INFOPATH            %{_infodir}
prepend-path    PKG_CONFIG_PATH     %{_libdir}/pkgconfig
EOF
%endif


%install
%if 0%{?rhel} >= 5 && 0%{?rhel} < 8
mkdir -p %{buildroot}/%{_scl_scripts}/root
install -c -p -m 0644 enable %{buildroot}%{_scl_scripts}/enable
%else
mkdir -p %{buildroot}%{_scl_scripts}
install -c -p -m 0644 %{scl} %{buildroot}%{_scl_scripts}
#automaticaly create enable script for compatibility
%scl_enable_script
%endif

%scl_install

# Drop %%scl definition, because we build various collection within single
# buildroot in copr where we can not (yet) have different minimal build-root
# package-set on per-package basis.
for file in %{buildroot}/%{_root_sysconfdir}/rpm/macros.%{scl}-config
do
    sed -i "s/%%scl.*/%%autotools_scl_build enabled/" "$file"
    cat "$file"
done

%files


%files build
%{_root_sysconfdir}/rpm/macros.%{scl}-config


%files runtime
%scl_files
%if 0%{?rhel} == 7
# Temporary fix for some bug in scl-utils-build-20130529-1.el7.x86_64
/opt/rh/autotools-git/root/lib64
%endif


%changelog
* Fri Feb 17 2017 Pavel Raiskup <praiskup@redhat.com> - 1-23
- bump for rhbz#1409277 (scl-utils built temporarily in praiskup/autotools)

* Tue Jan 03 2017 Pavel Raiskup <praiskup@redhat.com> - 1-22
- install macro file in 'build' subpackage, but without %%scl defined

* Fri Dec 30 2016 Pavel Raiskup <praiskup@redhat.com> - 1-21
- Fedora 26 added to copr

* Sun Oct 09 2016 Pavel Raiskup <praiskup@redhat.com> - 1-20
- don't depend on epel-rpm-macros, scl macros are not working with
  epel-rpm-macros unfortunately, provide build package

* Thu Sep 29 2016 Pavel Raiskup <praiskup@redhat.com> - 1-18
- cleanup a bit more with epel-rpm-macros
- BuildRoot is broken with epel-rpm-macros (rhbz#1379684)

* Tue Sep 27 2016 Pavel Raiskup <praiskup@redhat.com> - 1-17
- bump: rebuild september 2016
- depend on epel-rpm-macros

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 1-16
- Requires field must be spearated by spaces and not commas

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 1-15
- use _compat_el5_build only if defined (rhbz#1252751)

* Tue Jun 23 2015 Pavel Raiskup <praiskup@redhat.com> - 1-14
- make the meta-packages architecture dependant

* Tue Jun 23 2015 Pavel Raiskup <praiskup@redhat.com> - 1-13
- fix for scl-utils 2.0 (environment modules)

* Fri Aug 15 2014 Pavel Raiskup <praiskup@redhat.com> - 1-12
- rebuilt

* Wed May 28 2014 Pavel Raiskup <praiskup@redhat.com> - 1-11
- adjust also INFOPATH, use general pattern %%_compat_scl_env_adjust macro, also
  move generating of 'enable' script into %%build phase

* Fri Apr 18 2014 Pavel Raiskup <praiskup@redhat.com> - 1-10
- the fix for 'filelist' (#1079203) is not needed, according to
  https://fedorahosted.org/SoftwareCollections/ticket/18

* Thu Apr 17 2014 Pavel Raiskup <praiskup@redhat.com> - 1-9
- merge fixes with autotools-latest version

* Tue Mar 25 2014 Pavel Raiskup <praiskup@redhat.com> - 1-8
- buildroots are prepared, lets require all packages

* Mon Mar 24 2014 Pavel Raiskup <praiskup@redhat.com> - 1-7
- prepare for m4 incorporation (newer m4 is needed in RHEL5)

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 1-6
- fix project description a little

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 1-5
- ok, revert that hack back (this is needed for epel-7, not for rhel-7 buildroot
  atm)

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 1-4
- the hack for RHEL-7 is not needed now?

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 1-3
- temporarily disable autotools-git-libtool requirement

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 1-2
- workaround bug fixed in scl-utils-build-20130529-5

* Wed Mar 19 2014 Pavel Raiskup <praiskup@redhat.com> - 1-1
- initial packaging
