%{?_compat_el5_build}

# Workaround to always have %%scl defined (for testing locally)
%{!?scl:%global scl autotools-git}

%{?scl:%scl_package automake}

%global api_version 1.99
%global git_tag     6357a6

# remove once %%configure is used instead of ./configure
%{!?_pkgdocdir: %global _pkgdocdir %{_docdir}/%{name}-%{version}}

Summary:    A GNU tool for automatically creating Makefiles
Name:       %{?scl_prefix}automake
Version:    %{api_version}a
Release:    17.%git_tag%{?dist}

# docs ~> GFDL, sources ~> GPLv2+, mkinstalldirs ~> PD and install-sh ~> MIT
License:    GPLv2+ and GFDL and Public Domain and MIT

Group:      Development/Tools
Source:     ftp://ftp.gnu.org/gnu/automake/automake-%{version}.tar.gz
Source2:    http://git.savannah.gnu.org/cgit/config.git/plain/config.sub
Source3:    http://git.savannah.gnu.org/cgit/config.git/plain/config.guess

# Keep those patches in 'git format-patch' format (with docs).
Patch0:     automake-1.15-disable-vala-tests.patch

URL:        http://www.gnu.org/software/automake/
Requires:   %{?scl_prefix}autoconf >= 2.65

# requirements not detected automatically (#919810)
Requires:   perl(Thread::Queue)
Requires:   perl(threads)

Requires(post): /sbin/install-info
Requires(preun): /sbin/install-info
BuildArch:  noarch

# run "make check" by default
%bcond_with check

%{?scl:
BuildRequires:  scl-utils-build
Requires:       %scl_runtime
}

BuildRequires: %{scl_prefix}autoconf

# for better tests coverage:
%if %{with check}
#TODO: add libtool from SCL
BuildRequires: gettext-devel flex bison texinfo-tex
%if 0%{?rhel} != 5
BuildRequires: texlive-dvips
%endif
BuildRequires: java-devel-openjdk gcc-gfortran
# /usr/bin/g77 is not in RHEL7
%if !0%{?rhel}
%ifnarch %{arm} aarch64
# g77 is also not present on arm systems
BuildRequires: /usr/bin/g77
%endif
%endif
BuildRequires: dejagnu expect emacs imake python-docutils vala
BuildRequires: cscope ncompress sharutils help2man
BuildRequires: gcc-objc gcc-objc++
%if !0%{?rhel:1}
BuildRequires: python-virtualenv lzip
%endif
%endif

# Filtering macros are in perl-macros (fedora) and (perl-devel rhel6), not in
# RHEL5
%if 0%{?fedora} >= 20 || 0%{?rhel} >= 7
BuildRequires: perl-macros
%else
%if 0%{?rhel} != 5
# There is no macro filtering support on RHEL 5
BuildRequires: perl-devel
%endif
%endif
%if 0%{?fedora} >= 25 || 0%{?rhel} >= 8
BuildRequires:      perl-generators
%endif

# This is needed to allow 'help2man' to run 'automake --help' during build.
BuildRequires:   perl(Thread::Queue)

%if ! 0%{?rhel} == 5
# remove bogus Automake perl dependencies and provides
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(Automake::
%global __provides_exclude %{?__provides_exclude:%__provides_exclude|}^perl\\(Automake::
%endif


%description
Automake is a tool for automatically generating `Makefile.in'
files compliant with the GNU Coding Standards.

You should install Automake if you are developing software and would
like to use its ability to automatically generate GNU standard
Makefiles.

%prep
%setup -q -n automake-%{version}
%patch0 -p1 -b .disable_tests

# to use this, we should 100% *know* what the patches do
find -exec touch --date=`date -I` {} +

file=`find -name config.sub | head -1`
cp %{SOURCE2} $file
file=`find -name config.guess | head -1`
cp %{SOURCE3} $file

# Fedora only to add ppc64p7 (Power7 optimized) arch:
perl -pi -e "s/ppc64-\*/ppc64-\* \| ppc64p7-\*/" lib/config.sub

%build
%if 0%{?fedora} >= 21
# disable replacing config.guess and config.sub from redhat-rpm-config
%global _configure_gnuconfig_hack 0
%{?scl_enable}
%configure --docdir=%{_pkgdocdir}
make %{?_smp_mflags}
%{?scl_disable}
%else
# Use ./configure where rhbz#991613 is not fixed
%{?scl_enable}
./configure --prefix=%{_prefix} --mandir=%{_mandir} --infodir=%{_infodir} \
   --bindir=%{_bindir} --datadir=%{_datadir} --libdir=%{_libdir} \
   --docdir=%{_pkgdocdir} --disable-silent-rules
make V=0 %{?_smp_mflags}
%{?scl_disable}
%endif

cp m4/acdir/README README.aclocal
cp contrib/multilib/README README.multilib

%install
%_compat_install

%{?scl_enable}
make install DESTDIR=%{buildroot}
%{?scl_disable}

# %%doc is broken on older RHEL systems so that even if we set --docdir to
# %%_pkgdocdir by configure, procesing of %%doc removes whole %%_pkgdocdir and
# then it is filled by %%doc (relative) files.  This causes that amhello gets
# removed.
mv %{buildroot}/%{_pkgdocdir}/amhello* .
# Fail if non-empty!
rmdir %{buildroot}/%{_pkgdocdir}

%check
# %%global TESTS_FLAGS t/preproc-errmsg t/preproc-basics
%if %{with check}
make -k %{?_smp_mflags} check %{?TESTS_FLAGS: TESTS="%{TESTS_FLAGS}"} \
    || ( cat ./test-suite.log && false )
%endif

%post
/sbin/install-info %{_infodir}/automake.info.gz %{_infodir}/dir || :

%preun
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/automake.info.gz %{_infodir}/dir || :
fi

%files
%doc AUTHORS README THANKS NEWS README.aclocal README.multilib COPYING*
%doc amhello-1.0.tar.gz
%exclude %{_infodir}/dir
%exclude %{_datadir}/aclocal
%{_bindir}/*
%{_infodir}/*.info*
%{_datadir}/automake-%{version}
%{_datadir}/aclocal-%{version}
%{_mandir}/man1/*

%changelog
* Mon Oct 10 2016 Pavel Raiskup <praiskup@redhat.com> - 1.99a-17.6357a6
- BR perl-generators

- bump: rebuild october 2016

* Fri Jun 17 2016 Pavel Raiskup <praiskup@redhat.com> - 1.99a-14.6357a6
- rebase for latest git HEAD

* Wed Nov 04 2015 Pavel Raiskup <praiskup@redhat.com> - 1.99a-14.6357a6
- rebase for latest git HEAD

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 1.99a-13.904253
- sync with autotools-latest

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 1.99a-12.904253
- use _compat_el5_build only if defined (rhbz#1252751)

* Tue Jun 09 2015 Pavel Raiskup <praiskup@redhat.com> - 1.99a-11.904253
- BR perl-Thread-Queue to fix help2man run

* Sun Jun 15 2014 Pavel Raiskup <praiskup@redhat.com> - 1.99a-9.904253
- enable testsuite (don't fail though)
- fix BuildRequires for testsuite

* Thu May 29 2014 Pavel Raiskup <praiskup@redhat.com> - 1.99a-7.904253
- release bump for %%_compat_el5_build

* Fri May 23 2014 Pavel Raiskup <praiskup@redhat.com> - 1.99a-6
- rebase for git hash "90425304"

* Mon Mar 24 2014 Pavel Raiskup <praiskup@redhat.com> - 1.99a-5
- fixes for RHEL5

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 1.99a-4
- require scl-utils and scl-utils-build properly

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 1.99a-3
- same as autoconf, require perl-devel/perl-macros appropriately

* Fri Mar 21 2014 Pavel Raiskup <praiskup@redhat.com> - 1.99a-2
- most up2date automake requires autoconf 2.65+

* Wed Mar 19 2014 Pavel Raiskup <praiskup@redhat.com> - 1.99a-1
- SCL-ized spec from Rawhide, no testsuite yet
