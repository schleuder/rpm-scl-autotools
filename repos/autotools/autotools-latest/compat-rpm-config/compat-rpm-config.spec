## WARNING: epel-rpm-macros needs to be installed within minimal epel chroot

%{?_compat_el5_build}

# Allow installing of this package on RHEL5
%global _binary_filedigest_algorithm md5

%global rhmacrodir /etc/rpm/

Name:		compat-rpm-config
Version:	1.0
Release:	12%{?dist}
Summary:	Cross-distro compat macros

License:	GPLv3+
URL:		TODO
Source0:	macros.compat
Group:		Development/System


BuildRequires:	redhat-rpm-config
Requires:	redhat-rpm-config
BuildArch:	noarch

%description
This package contains set of "build" RPM macros to make the cross-distribution
building workarounds easier.  There are limited guarantees of API stability --
it lives in Copr only (for now?).  If you plan use some of implemented macros in
your package, contact 'praiskup' to discuss stabilizing (macros are used only by
builds of his packages in copr so far).


%prep


%build


%install
mkdir -p %{buildroot}%{rhmacrodir}
cp %{SOURCE0} %{buildroot}%rhmacrodir


%files
%rhmacrodir/*


%changelog
* Sat Dec 31 2016 Pavel Raiskup <praiskup@redhat.com> - 1.0-12
- Fedora 26 added

* Tue Sep 27 2016 Pavel Raiskup <praiskup@redhat.com> - 1.0-11
- try to not require ourselves

* Wed Aug 12 2015 Pavel Raiskup <praiskup@redhat.com> - 1.0-10
- conditional _compat_el5_build to allow 'rpmbuild -bs' from copr dist-git

* Fri Jul 18 2014 Pavel Raiskup <praiskup@redhat.com> - 1.0-9
- rebuild

* Wed May 28 2014 Pavel Raiskup <praiskup@redhat.com> - 1.0-8
- add _compat_scl_env_adjust macro

* Sun May 25 2014 Pavel Raiskup <praiskup@redhat.com> - 1.0-7
- rebuilt

* Sun May 25 2014 Pavel Raiskup <praiskup@redhat.com> - 1.0-5
- binary digest algorithm should be md5 to allow install on rhel5

* Sun May 25 2014 Pavel Raiskup <praiskup@redhat.com> - 1.0-4
- %_compat_br_perl_macros added
- rename file name to macros.compat
- reuse macros from this package

* Sat May 24 2014 Pavel Raiskup <praiskup@redhat.com> - 1.0-3
- make the package little bit less author oriented

* Fri May 23 2014 Pavel Raiskup <praiskup@redhat.com> - 1.0-2
- fixes for el5 build

* Fri May 23 2014 Pavel Raiskup <praiskup@redhat.com> - 1.0-1
- initial packaging
